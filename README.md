# Metrolink NextTram

Tells you the next tram departure times from any station on the Manchester metrolink.

![Example output](example1.png)

### Packages used

python-dotenv   
https://pypi.org/project/python-dotenv/

## Setup

All data provided by TFGM. You must register an account and gain access to their API (free to use).  
Developer portal:  
https://developer.tfgm.com/  

### By command line
Use arguments `--station` and `--api_key`

### By env file
Copy `.env.example` into a new file, `.env`. Enter your key and station (spaces don't need to be quoted).  

## Run 
`python metrolink.py`   

You can pass `--outbound` to get times for trams going in the opposite direction
Pass `--short` for a more concise output

### Protip  
Use the `watch` command to make this into a live feed.  
e.g `watch -n 15 python metrolink.py --station Cornbrook` would refresh every 15 seconds

Hope somebody finds this useful. Enjoy!

## TODO
- [ ] Print last tram time
