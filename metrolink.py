import http.client, urllib.request, urllib.parse, urllib.error, base64
import os, sys
import json, argparse

from dotenv import load_dotenv

load_dotenv()

parser = argparse.ArgumentParser(description='Prints metrolink times and info for your station')

parser.add_argument('--station', type=str,
                    help='The station you are travelling from')

parser.add_argument('--api_key', type=str,
                    help='The api key')

parser.add_argument('--outbound', action='store_true',
                    help='Pass this to view times for the outbound platform (away from Manchester)')

parser.add_argument('--short', action='store_true',
                    help='Print the time & shortened station name')

args = parser.parse_args()

station_name = os.environ['STATION_NAME'] if not args.station else args.station
api_key = os.environ['TFGM_SUBSCRIPTION_KEY'] if not args.api_key else args.api_key

class Tram(object):
    destination = ""
    wait = ""
    message = ""

    def __init__(self, destination, wait, message):
        self.destination = destination
        self.wait = wait
        self.message = message

    def print_formatted(self):
        if self.destination != "" and self.wait != "":
            print(f'{self.destination} {self.wait} min')

    def print_message(self):
        if self.message != "":
            print(f'{self.message}')

    def print_short(self):
        if self.destination != "" and self.wait != "":
            print(f'{self.wait} {self.destination[:3]}')

def print_tram_times(trams):
    print(f'Tram times from {station_name}')
    print('=================')

    for tram in trams:
        tram.print_formatted()

    print('=================')
    # Messages are likely the same for every tram. Just print the first one
    trams[0].print_message()

def print_short_tram_times(trams):
    for tram in trams:
        tram.print_short()

def parse_response(response_json):
    response = json.loads(response_json)['value']
    
    platform = None
    for res_platform in response:
        direction = 'Incoming' if not args.outbound else 'Outgoing'
        if res_platform['Direction'] == direction: 
            platform = res_platform

    # TODO Loop over these incase there's more than 3. More of a problem for stations with multiple lines
    tram_1 = Tram(platform['Dest0'], platform['Wait0'], platform['MessageBoard'])
    tram_2 = Tram(platform['Dest1'], platform['Wait1'], platform['MessageBoard'])
    tram_3 = Tram(platform['Dest2'], platform['Wait2'], platform['MessageBoard'])

    return (tram_1, tram_2, tram_3)

headers = {
    'Ocp-Apim-Subscription-Key': api_key,
}

params = urllib.parse.urlencode({
    # Request parameters
    # '$expand': '{string}',
    # '$select': '{string}',    
    '$filter': 'StationLocation eq \'%s\''%(station_name),
    # '$orderby': '{string}',
    # '$top': '10',
    # '$skip': '{integer}',
    # '$count': '{boolean}',
})

def get_data():
    try:
        conn = http.client.HTTPSConnection('api.tfgm.com')
        conn.request("GET", "/odata/Metrolinks?%s" % params, "{body}", headers)
        response = conn.getresponse()
        data = response.read()
        conn.close()
        # print(data)
        return data
    except Exception as e:
        print("[Errno {0}] {1}".format(e.errno, e.strerror))

if __name__ == '__main__':
    if not api_key or not station_name:
        sys.exit('Please provide the api key and station name')

    # Getting data
    data = get_data()
    tram_info = parse_response(data)

    # Print
    if not args.short:
        print_tram_times(tram_info)
    else:
        print_short_tram_times(tram_info)
